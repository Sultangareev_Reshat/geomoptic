#include "myline.h"

MyLine::MyLine()
{
    //Собственный класс для линий, так как только для GraphicsItem работает collision
    p1 = new QPoint();
    p2 = new QPoint();
    p1->setX(0);
    p1->setY(0);
    p2->setX(10);
    p2->setY(10);
    QVariant v(120);
    this->setData(9,v);

}

void MyLine::setP1(QPoint p11)
{
    p1->setX(p11.x());
    p1->setY(p11.y());
}

void MyLine::setP2(QPoint p22)
{
    p2->setX(p22.x());
    p2->setY(p22.y());
}

void MyLine::setP(QPoint p11, QPoint p22)
{

    p1->setX(p11.x());
    p1->setY(p11.y());
    p2->setX(p22.x());
    p2->setY(p22.y());
    k = (double)(p2->y()-p1->y())/(double)(p2->x()-p1->x());
    c =  (double)(-1)*p1->x()*(p2->y()-p1->y())/(p2->x() - p1->x())+ (double)p1->y();
    update();
    //qDebug() << p11 << p22 <<"Коэфицент к и с" <<k << c;
}

QRectF MyLine::boundingRect() const
{

    return QRectF(0,0,p2->x(), p2->y());
}

QPainterPath MyLine::shape() const
{

    //Создает саму линию
    QPainterPath path;
    QPolygon polygon;
    polygon << QPoint(p1->x(), p1->y());

    polygon << QPoint(p2->x(), p2->y());



    path.addPolygon(polygon);

    return path;
}

void MyLine::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    // Рисует линию
    QPainterPath pt = shape();
    mapToScene(pt);

    painter -> drawPath(pt);
}

void MyLine::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    //Пока просто передает ивент родительскому классу
    QGraphicsItem::mouseReleaseEvent(event);
}
