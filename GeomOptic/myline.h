#ifndef MYLINE_H
#define MYLINE_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QList>
#include <lense.h>

class MyLine : public QGraphicsItem
{
public:
    explicit MyLine();
    int width;
    int height;
    QPoint *p1;
    QPoint *p2;
    double k;
    double c;
    int state = 0;
    //1 - lower line
    //2 - higher line

public:
    void setP1(QPoint p1);
    void setP2(QPoint p2);
    void setP(QPoint p1, QPoint p2);

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // QGraphicsItem interface
public:

protected:
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
};

#endif // MYLINE_H
