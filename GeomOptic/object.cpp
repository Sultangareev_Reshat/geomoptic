#include "object.h"
#include "lense.h"
#include "mainwindow.h"


Object::Object()
{
    pressed = false;
    int StartX = 800/2;
    int StartY = 800/2;

    setPos(mapToParent(StartX, StartY));
    // Заполняем данными объект, чтобы потом его можно было опознать
    //5 это ключ а 122 записываемые данные
    QVariant v(122);
    this -> setData(5,v);
}

QRectF Object::boundingRect() const
{
    //Возващает границы объкта, нужно для памяти и перересовки
    return QRect(0, 0, width, height);
}

void Object::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rec = boundingRect();

    QBrush Brush(Qt::green);

    if(pressed)
        Brush.setColor(Qt::yellow);

    painter -> fillRect(rec, Brush);
    painter -> drawRect(rec);
    scene()->update();
}

void Object::DoCollision()
{

}

QList<Lense *> Object::GetLenses()
{

    QList<QGraphicsItem*> sss = scene()->items();
    QList<Lense*> list;
    QVariant v(123);
    int CurrentItem = 0;
    //sss[i]->data(5) это функция позволяет хранить некоторые данные в объекте
    Lense *Uk = nullptr;
    qreal MinX = 800;
    int removNum = -1;
    Lense *ls;
    do
    {
    Uk = nullptr;
    MinX = 10000;
    for(int i = 0; i <  scene()->items().count(); ++i)
    {
        if(sss[i]->data(5) == v)
        {
            //Пытаюсь переделать в линзу, если удачно, записываем
            ls = dynamic_cast<Lense*>(sss[i]);
            if(ls->x() < MinX)
            {
                removNum = i;
                MinX = ls->x();
                Uk = ls;
            }
        }
    }
    if(Uk != nullptr)
    {

    list.append(Uk);
    }
    if(removNum != -1)
    sss[removNum]->setData(5,0);
    }
    while(Uk != nullptr);

    for(int i = 0; i < list.count();++i)
    {
        list[i]->setData(5,123);
    }



    return list;
}


void Object::mousePressEvent(QGraphicsSceneMouseEvent *event)
{



    pressed = true;
    QGraphicsItem::mousePressEvent(event);

    QList<QGraphicsItem*> sss = scene()->items();
    for(int i = 0; i <  sss.count(); ++i)
    {
        MyLine *ls = dynamic_cast<MyLine*>(sss[i]);
        if(ls)
            scene()->removeItem(ls);


    }

    for(int i = 0; i <  sss.count(); ++i)
    {

        Object *lr = dynamic_cast<Object*>(sss[i]);
        if(lr)
            if(lr->isReflection)
                scene()->removeItem(lr);
    }

    update();

}
bool Object::isOnLine(MyLine *line, qreal x, qreal y)
{
    //Проверка лежит ли точка на линии(на квадрате)
    if((line->p1->x() > x && line->p2->x() < x) || (line->p2->x() > x && line->p1->x() < x))
    {

    }
    else return 0;
    if((line->p1->y() >= y && line->p2->y() <= y) || (line->p2->y() >= y && line->p1->y() <= y))
    {

    }
    else return 0;

return true;


}

void Object::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    pressed = false;
    QList<Lense*> lenses;
    QList<QGraphicsItem*> collidingItems;

    //collidingItems Получает список всего с чем сталкивается объект
    collidingItems = this ->collidingItems();

    for(int i = 0; i < collidingItems.count();++i)
    {
        if(collidingItems[i]->data(5) == 122)
        {
        }
        if(collidingItems[i]->data(5) == 123)
        {


        }
        else
        {


        }

    }


    int x, y;
    QPoint p1(this->scenePos().x(),this->scenePos().y());
    QPoint p2(800,this->scenePos().y());

    QPoint p1lower(this->scenePos().x(),this->scenePos().y() + height);
    QPoint p2lower(800,this->scenePos().y() + height);

    //Выделение памяти и создание моей линии параллельной х
    MyLine *upperLeft = new MyLine();
    MyLine *lowerLeft = new MyLine();


    upperLeft->setP(p1, p2);
    lowerLeft->setP(p1lower, p2lower);
    lowerLeft->state = 1; // lower
     scene() -> addItem(lowerLeft);
     scene()->update();

     upperLeft->state = 2; // higher

     scene() -> addItem(upperLeft);
     scene()->update();


    // lenses заполняется всеми найденными линзами на сцене
     lenses = GetLenses();

     MyLine *secondline= new MyLine();
     //Ищем первую линзу
     int min = 800;
     int firstLense = 0;

     for(int i = 0; i < lenses.count(); ++i)
     {
         if(lenses[i]->x() < min)
         {
             min = lenses[i]->x();
             firstLense = i;

         }
     }

     if(!lenses.isEmpty())
     {
     QPoint p3(lenses[firstLense]->scenePos().x()-lenses[firstLense]->focus, lenses[firstLense]->scenePos().y()+lenses[firstLense]->height/2);


     secondline->setP(p1,p3);

    scene()->update();

    //Проверка на наличее линз Верх
     }
    if(!lenses.isEmpty())
    {
        x = 800;
        y = (x - this->scenePos().x())*(lenses[firstLense]->pos().y() + lenses[firstLense]->height/2 -this->scenePos().y())/(lenses[firstLense]->pos().x()-this->scenePos().x()) + this->scenePos().y();

        if(y > 800 || y < 0)
        {
            y = 800;
            x = (y - this->scenePos().y())*(lenses[firstLense]->pos().y()+ lenses[firstLense]->height/2 -this->scenePos().x())/(lenses[firstLense]->pos().x()-this->scenePos().y()) + this->scenePos().x();
        }
        if(x > 800 || x < 0)
        {
            x = 800;
            y = (x - this->scenePos().x())*(lenses[firstLense]->pos().x()+ lenses[firstLense]->height/2 -this->scenePos().y())/(lenses[firstLense]->pos().y()-this->scenePos().x()) + this->scenePos().y();
        }
        QPoint p11(this->scenePos().x(),this->scenePos().y());
        QPoint p22(x,y);
        //Создание линии центральной
        MyLine *pp = new MyLine();
        pp->setP(p11,p22);
        pp->state = 2;
        scene() ->addItem(pp);
        scene()->update();


    }
    Sleep(50);

    //Проверка на наличее линз Низ

    if(!lenses.isEmpty())
    {
        x = 800;
        y = (x - this->scenePos().x())*(lenses[firstLense]->pos().y() + lenses[firstLense]->height/2 -this->scenePos().y() - this->height)/(lenses[firstLense]->pos().x()-this->scenePos().x()) + this->scenePos().y() + this->height;

        if(y > 800 || y < 0)
        {
            y = 800;
            x = (y - this->scenePos().y() + this->height)*(lenses[firstLense]->pos().y()+ lenses[firstLense]->height/2 -this->scenePos().x())/(lenses[firstLense]->pos().x()-this->scenePos().y() + this->height) + this->scenePos().x();
        }
        if(x > 800 || x < 0)
        {
            x = 800;
            y = (x - this->scenePos().x())*(lenses[firstLense]->pos().x()+ lenses[firstLense]->height/2 -this->scenePos().y() + this->height)/(lenses[firstLense]->pos().y()-this->scenePos().x()) + this->scenePos().y() + this->height;
        }
        QPoint p11lower(this->scenePos().x(),this->scenePos().y() + this->height);
        QPoint p22lower(x,y);
        //Создание линии центральной
        MyLine *pplower = new MyLine();

        pplower->setP(p11lower,p22lower);
        pplower->state = 1;
         scene() ->addItem(pplower);
         scene() ->update();



    }




    for(int i = 0; i < lenses.count() ;++i)
    {
    lenses[i]->CollidLense();
    update();
    }
    //Ищем все линии

    QList<QGraphicsItem*> allitems = scene()->items();
    QList<MyLine*> mlines;

    for(int i = 0; i < allitems.count(); ++i)
    {

        MyLine *lml = dynamic_cast<MyLine*>(allitems[i]);
        if(lml)
        {
            mlines.append(lml);
        }

    }

    //Ищем пересечания
    int interceptionX = 0;
    int interceptionY = 0;
    int upx, upy, lowx, lowy;
    upx = upy = lowx = lowy = 0;
    bool Good = false;
    for(int i = 0; i < mlines.count(); ++i)
        {
            for(int k = i; k < mlines.count(); ++k)
            {
                interceptionX =(mlines[i]->c - mlines[k]->c)/(mlines[k]->k - mlines[i]->k);

                interceptionY = mlines[k]->k*interceptionX + mlines[k]->c;

                if(interceptionX > 800 || interceptionX < 0)
                    interceptionX = 0;
                if(interceptionY > 800 || interceptionY < 0)
                    interceptionY = 0;
                if(interceptionX != 0 && interceptionY != 0)
                {

                }

          if(mlines[i]->state == mlines[k] -> state)
           if(isOnLine(mlines[i],interceptionX, interceptionY) && isOnLine(mlines[k],interceptionX, interceptionY))
                if(interceptionX > this->pos().x() + width)
                {
                    if(mlines[i]->state == 1)
                    {
                        lowx = interceptionX;
                        lowy = interceptionY;
                    }
                    else if(mlines[i]->state == 2)
                    {
                        upx = interceptionX;
                        upy = interceptionY;
                    }
                    if(lowx && lowy && upx && upy) // если все пересечения найдены
                    {
                        Good = true;
                    }
                }
          if(Good)
          {
              Object *refl = new Object();

              if(upy > lowy)
              {
              refl->height = upy - lowy;
              refl->setX(upx);
              refl->setY(upy - refl->height);
              refl->isReflection =true;
              scene()->addItem(refl);
              }
              else
              {
                  refl->height = lowy - upy;
                  refl->setX(lowx);
                  refl->setY(lowy - refl->height);
                  //refl->width = lowx - upx;
                  refl->isReflection =true;
                  scene()->addItem(refl);

              }
              upx = upy = lowx = lowy = 0;
              Good = false;
          }
            }
        }

    //Передача ивента родительскому классу

    QGraphicsItem::mouseReleaseEvent(event);
}

