#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "add_an_object.h"
#include "ui_add_an_object.h"
#include "myline.h"
Add_an_object::Add_an_object(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Add_an_object)
{
    ui->setupUi(this);
}

Add_an_object::~Add_an_object()
{
    delete ui;
}

void Add_an_object::on_pushButton_clicked()
{
   global_height=ui->lineEdit->text().toDouble();
   close();
}
