#include "add_a_lense.h"
#include "ui_add_a_lense.h"
#include "mainwindow.h"
add_a_lense::add_a_lense(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_a_lense)
{
    ui->setupUi(this);
}

add_a_lense::~add_a_lense()
{
    delete ui;
}

void add_a_lense::on_pushButton_clicked()
{
    global_height_lense=ui->lineEdit->text().toDouble();
    global_focus=ui->lineEdit_2->text().toDouble();
    close();
}
