#ifndef ADD_AN_OBJECT_H
#define ADD_AN_OBJECT_H

#include <QMainWindow>
#include <QStatusBar>
#include <QPainter>
#include <QtGui>
#include <QtCore>
#include <QDialog>
#include<QGraphicsScene>
#include <QMessageBox>
#include <QDebug>

#include "object.h"
#include "lense.h"
#include "myline.h"
namespace Ui {
class Add_an_object;
}

class Add_an_object : public QDialog
{
    Q_OBJECT

public:
    explicit Add_an_object(QWidget *parent = nullptr);
    ~Add_an_object();
    Object *Item;

    Lense *Lenses;

private slots:
    void on_pushButton_clicked();

private:
    Ui::Add_an_object *ui;
};

#endif // ADD_AN_OBJECT_H
