#include "lense.h"
#include "mainwindow.h"
#include <QRandomGenerator>
Lense::Lense()
{
        pressed = false;
        int StartX = 800/2;
        int StartY = 800/2 - height/2;
        focus = qrand()%30 + 70;
        QVariant v(123);
        this -> setData(5,v);

        setPos(mapToParent(StartX, StartY));
}

void Lense::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    pressed = true;
    QGraphicsItem::mousePressEvent(event);
    update();
}

void Lense::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    pressed = false;
    QGraphicsItem::mouseReleaseEvent(event);
    update();

}

void Lense::CollidLense()
{
    QList<QGraphicsItem*> lenses;
    QList<QGraphicsItem*> collidingItems;

    scene() ->update();
    scene() ->update();

    collidingItems = this->collidingItems();

    for(int i = 0; i < collidingItems.count();++i)
    {

        if(collidingItems[i]->data(5) == 122)
        {
           //"collides with square";
        }
        if(collidingItems[i]->data(5) == 123)
        {
           // qDebug() << "collides with lense";
        }
        if(collidingItems[i]->data(9) == 120 )
        {
            //collides with myLine
            MyLine *lnNOm = dynamic_cast<MyLine*>(collidingItems[i]);

            MyLine *ln = new MyLine();

            if( static_cast<int>(lnNOm->k*this->x() + lnNOm->c + 0.5) == static_cast<int>(this->y() + height/2))
                continue;



            int y;
            //Получение с линии коэфицентов k и c, y =kx + c;
            y = lnNOm->k * this->pos().x() +  lnNOm->c;

            //Точка пересечения линзы и линии
            QPoint nwp(this->pos().x(), y);

            //Удаление старой линии(Она не преломляется)
            scene()->removeItem(lnNOm);

            //Передача информации от какой части объекта пришла линия
            ln->state = lnNOm->state;

            ln->setP(*lnNOm->p1, nwp);

            //Уменьшенная линия от объекта до линзы
            ln->setData(11,11);

            scene()->addItem(ln);

            scene()->update();

            QPoint p2;
            //Подсчет линии от линзы до нового объекта
            QPoint p1(this->pos().x(),y);
            if(this->kind == 1)
            {
            p2.setX(this->pos().x() + focus);
            p2.setY(this->pos().y() + height/2);
            }
            if(this->kind == 2)
            {
                p2.setX(this->pos().x() - focus);
                p2.setY(this->pos().y() + height/2);
            }



            MyLine *lne = new MyLine();

            lne -> setP(p1,p2);


            //Подставляем в полученное уравнение x = 800(Граница экрана)
            int x1 = 800;
            int y1 = lne->k * x1 + lne->c;
            p2.setX(x1);
            p2.setY(y1);

            MyLine *lneN = new MyLine();


            lneN -> setP(p1,p2);
            lneN->state = lnNOm->state;

            //Добавляем линию на экран
            scene()->addItem(lneN);

            update();

        }
        else
        {

        }

    }

}

void Lense::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    //Принимает форму квадрата
    QRectF rec = boundingRect();

    QBrush Brush(Qt::blue);

    if(pressed)
    {
        Brush.setColor(Qt::red);
    }
    painter -> fillRect(rec, Brush);
    painter -> drawRect(rec);
}

QRectF Lense::boundingRect() const
{
    return QRect(0,0,width,height);
}
