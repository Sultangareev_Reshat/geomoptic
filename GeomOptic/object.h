#ifndef OBJECT_H
#define OBJECT_H

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QDebug>
#include <QWidget>
#include <QGraphicsSceneMouseEvent>
#include <QList>
#include <lense.h>
#include <myline.h>

class Object : public QGraphicsItem, public QObject
{
public:
    Object();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
    bool pressed;
    void DoCollision();
    QList<Lense*> GetLenses();
    bool isOnLine(MyLine *line, qreal x, qreal y);

    double height = 20;
    double width = 5;


    bool isReflection = false;

protected:

    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

};

#endif // OBJECT_H
