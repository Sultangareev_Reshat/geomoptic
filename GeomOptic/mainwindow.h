#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStatusBar>
#include <QPainter>
#include <QtGui>
#include <QtCore>
#include <QDialog>
#include<QGraphicsScene>
#include <QMessageBox>
#include <QDebug>

#include "object.h"
#include "lense.h"
#include "myline.h"
extern double global_height;
extern double global_height_lense;
extern double global_focus;
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    virtual void paintEvent(QPaintEvent *event);

    int Draw = 0;

    Object *Item[50];

    Lense *Lenses[50];


    int CurrentItem = 0;

    QGraphicsScene *scene;
public slots:


private slots:

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_action_triggered();

    void on_action_4_triggered();
    QList<Lense *> GetLenses();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
