#ifndef LENSE_H
#define LENSE_H

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QGraphicsItem>
#include <QObject>
#include <QDebug>
#include "myline.h"
class Lense : public QGraphicsItem, public QObject
{
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
    bool pressed;
    int N = 1;
public:
    QRectF boundingRect() const override;
    Lense();
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    double focus = 50;
    double focus2=-50;
    double height = 100;
    double width = 10;
    int kind = 1; // собирающая
    // kind = 2 рассеюваящая
    int last=0;
    // QGraphicsItem interface
public:
    void CollidLense();
};

#endif // LENSE_H
