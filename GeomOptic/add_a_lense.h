#ifndef ADD_A_LENSE_H
#define ADD_A_LENSE_H

#include <QMainWindow>
#include <QStatusBar>
#include <QPainter>
#include <QtGui>
#include <QtCore>
#include <QDialog>
#include<QGraphicsScene>
#include <QMessageBox>
#include <QDebug>

#include "object.h"
#include "lense.h"
#include "myline.h"

namespace Ui {
class add_a_lense;
}

class add_a_lense : public QDialog
{
    Q_OBJECT

public:
    explicit add_a_lense(QWidget *parent = nullptr);
    ~add_a_lense();
    Object *Item;

    Lense *Lenses;
private slots:
    void on_pushButton_clicked();

private:
    Ui::add_a_lense *ui;
};

#endif // ADD_A_LENSE_H
