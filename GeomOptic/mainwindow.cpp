#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "add_an_object.h"
#include "ui_add_an_object.h"
#include "add_a_lense.h"
#include "ui_add_a_lense.h"
#include "myline.h"
#include "lense.h"
#include "object.h"

double global_height=30;
double global_height_lense=100;
double global_focus=50;
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QPen mypen = QPen(Qt::blue);

    //Создание вертикальной и горизонатльной линии
    QPoint pointtl =mapToGlobal(this->rect().topLeft());
    QPoint pointtr = mapToGlobal(this->rect().topRight());
    QPoint pointll = mapToGlobal(this->rect().bottomLeft());
    QPoint pointlr = mapToGlobal(this->rect().bottomRight());


    QPoint pointuc(pointtr.x()/2, pointtr.y());
    QPoint pointlc(pointlr.x()/2, pointlr.y());

    QPoint point1(pointlr.x(), pointlr.y()/2);
    QPoint point2(pointll.x(), pointll.y()/2);

    QLine linevert;
    linevert.setPoints(point1, point2);
    QLine linehz;
    linehz.setPoints(pointuc, pointlc);
    QLine line1;
    line1.setPoints(pointtl,pointtr);
    QLine line2;
    line2.setPoints(pointtl,pointll);
    QLine line3;
    line3.setPoints(pointtr,pointlr);
    QLine line4;
    line4.setPoints(pointll,pointlr);
    scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);


    //Создание самой сцены
    ui -> graphicsView -> setScene(scene);
    ui -> graphicsView -> setRenderHint(QPainter::Antialiasing);
    //Добавление линий на сцену
    scene ->addLine(linehz);
    scene ->addLine(linevert);

    //Линии вокруг границ сцены
    scene ->addLine(line1);
    scene ->addLine(line2);
    scene ->addLine(line3);
    scene ->addLine(line4);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    //Создание нашего объекта
    Item[CurrentItem] = new Object();

    Item[CurrentItem]->height=global_height;

    scene->addItem(Item[CurrentItem]);
    Item[CurrentItem] -> setFlag(QGraphicsItem::ItemIsMovable);
    CurrentItem++;

}
void MainWindow::paintEvent(QPaintEvent *event)
{

}

int number=1;
void MainWindow::on_pushButton_2_clicked()
{
    //Создание линзы
    Lense *ls = new Lense();
    ls->last=number;
    ls->height=global_height_lense;
    ls->focus=global_focus;
    number++;
    scene->addItem(ls);
    ls -> setFlag(QGraphicsItem::ItemIsMovable);

}


void MainWindow::on_pushButton_4_clicked()
{
    //добавление расс линзы
    Lense *ls = new Lense();
    ls->kind = 2;
    ls->last=number;
    ls->height=global_height_lense;
    ls->focus=global_focus;
    number++;
    scene->addItem(ls);
    ls -> setFlag(QGraphicsItem::ItemIsMovable);

}

void MainWindow::on_pushButton_5_clicked()
{
    CurrentItem--;
    if(CurrentItem>=0)
    {
        scene->removeItem(Item[CurrentItem]);
        QList<QGraphicsItem*> sss = scene->items();
        for(int i = 0; i <  sss.count(); ++i)
        {
            MyLine *ls = dynamic_cast<MyLine*>(sss[i]);
            if(ls)
                scene->removeItem(ls);


        }

        for(int i = 0; i <  sss.count(); ++i)
        {

            Object *lr = dynamic_cast<Object*>(sss[i]);
            if(lr)
                if(lr->isReflection)
                    scene->removeItem(lr);
        }
        scene->update();
        update();
    }
    else
    {
        CurrentItem=0;
    }
    scene->update();
    scene->update();
    update();
}

QList<Lense *> MainWindow::GetLenses()
{

    QList<QGraphicsItem*> sss = scene->items();
    QList<Lense*> list;
    QVariant v(123);
    int CurrentItem = 0;
    //sss[i]->data(5) это функция позволяет хранить некоторые данные в объекте
    Lense *Uk = nullptr;
    qreal MinX = 800;
    int removNum = -1;
    Lense *ls;
    do
    {
    Uk = nullptr;
    MinX = 10000;
    for(int i = 0; i <  scene->items().count(); ++i)
    {
        if(sss[i]->data(5) == v)
        {
            //Пытаюсь переделать в линзу, если удачно, записываем
            ls = dynamic_cast<Lense*>(sss[i]);
            if(ls->x() < MinX)
            {
                removNum = i;
                MinX = ls->x();
                Uk = ls;
            }
        }
    }
    if(Uk != nullptr)
    {

    list.append(Uk);
    }
    sss[removNum]->setData(5,0);
    }
    while(Uk != nullptr);

    for(int i = 0; i < list.count();++i)
    {
        list[i]->setData(5,123);
    }



    return list;
}
void MainWindow::on_pushButton_6_clicked()
{
    if(number>1)
    {
        QList<Lense *> lenses=GetLenses();
        for(int i=0; i<number-1; i++)
        {
             if(lenses[i]->last==number-1)
              {
                scene->removeItem(lenses[i]);
                scene->update();
                number--;
               }
            }
    }
    scene->update();
    update();

}

void MainWindow::on_action_triggered()
{
    Add_an_object window;

    window.setModal(true);
    window.exec();

}

void MainWindow::on_action_4_triggered()
{
    add_a_lense window;

    window.setModal(true);
    window.exec();
}
